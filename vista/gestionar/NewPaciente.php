<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Clinica Universitaria</title>
    <link rel = "stylesheet" href = ".././css/style.css">
  </head>
  <body>
    <div class = "header">
      <a href= "#default" class = "MedicinaGen">Medicina General</a>
      <a href= "#default" class = "Farmacia">Farmacia</a>
      <a href= "#default" class = "Informes">Informes</a>
        <div class = "header-right">
          <a class="btn btn-primary" href="#Search" role="button"><img src = ".././vista/images/buscar.png" class = "logomin"></a>
          <a href = "#Username"> Username </a>
          <a class="btn btn-primary" href="#Profile" role="button"><img src = ".././vista/images/user2.png" class = "logomin"></a>
          <a class="btn btn-primary" href="#Config" role="button"><img src = ".././vista/images/settings.png" class = "logomin"></a>
        </div>
    </div>
  </body>
</html>
