<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Clinica Universitaria</title>
    <link rel = "stylesheet" href = ".././css/style.css">
  </head>
  <body>
    <?php
  session_start();
  if(!isset($_SESSION["user"])){
    header("location:login.php");
  }else{

  }
   ?>
    <div class = "header">
      <a href= "#default" class = "MedicinaGen">Medicina General</a>
      <a href= "#default" class = "Farmacia">Farmacia</a>
      <a href= "#default" class = "Informes">Informes</a>
        <div class = "header-right">
          <a class="btn btn-primary" href="#Search" role="button"><img src = ".././vista/images/buscar.png" class = "logomin"></a>
          <a href = "#Username"> Username </a>
          <a class="btn btn-primary" href="#Profile" role="button"><img src = ".././vista/images/user2.png" class = "logomin"></a>
          <a class="btn btn-primary" href="#Config" role="button"><img src = ".././vista/images/settings.png" class = "logomin"></a>
        </div>
    </div>
    <div class = "container">
      <a class = "text-inside">Todos los pacientes</a>
        <div class = "container-right">
        <a class = "btn btn-primary" href = "./gestionar/NewPaciente.php" role = "button">Nuevo Paciente</a>
        <a class = "btn btn-primary" href = "#Filter" role = "button">...</a>
        </div>
  </div>
  <div class "container">
  </div>
  <div class = "tab">
    <div class = "box">
      <div class = "selected">
        <a class = "text">FILTRAR PACIENTES POR</a>
      </div>
    </div>
    <br><input type = "radio" name = "Sport" value = "Deporte"> Deporte</br>
    <br><input type = "radio" name = "Patient_Name" value = "Nombre del Paciente"> Nombre del Paciente</br>
    <br><input type = "radio" name = "Sport" value = "Facultad"> Facultad</br>
    <br><input type = "radio" name = "Sport" value = "Carrera"> Carrera</br>
    <br><input type = "radio" name = "Sport" value = "Semestre"> Semestre</br>
    <br><input type = "radio" name = "Sport" value = "Año"> Año</br>
    <br><input type = "radio" name = "Sport" value = "No. de Identificacion"> No. de Identificacion</br>
    <br><input type = "radio" name = "Sport" value = "No. de Carnet de Becado"> No. de Carnet de Becado</br>
    <br><input type = "radio" name = "Sport" value = "Sexo"> Sexo</br>
    <br><input type = "radio" name = "Sport" value = "Edad"> Edad</br>
    <br><input type = "radio" name = "Sport" value = "Correo"> Correo</br>
    <br><input type = "radio" name = "Sport" value = "Telefono"> Telefono</br>
  </div>
  <div class = "flex-container">
    <div>
      <a class = "text"> NOMBRE DE PACIENTE</a>
    </div>
    <div>
      <a class = "text"> CARRERA</a>
    </div>
    <div>
      <a class = "text"> EXPEDIENTE</a>
    </div>
    <div>
      <a class = "text"> TELEFONO</a>
    </div>
    <div>
      <a class = "text"> DEPORTE</a>
    </div>
  </div>
  <div class = "down-tab">
    <input type = "radio" name = "Patient" value = "Patient"> José Daniel Loaisiga
  </div>
  </body>
</html>
