<!DOCTYPE hmtl>
<html>
<head>
  <meta http-equiv="content-type" content="text/html"; charset="utf-8"/>
  <title> Documento sin titulo </title>
</head>
<body>
  <?php
    try{
      $base = new PDO("mysql:host=localhost; dbname=clinica_unan_managua", "root", "");
      $base->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $sql = "SELECT * FROM user_person WHERE username = :username AND pass= :password";

      $resultado= $base->prepare($sql);

      $username=htmlentities(addslashes($_POST["username"]));
      $password=md5(htmlentities(addslashes($_POST["password"])));

      $resultado->bindValue(":username", $username);
      $resultado->bindValue(":password", $password);

      $resultado->execute();

      $numero_registro=$resultado->rowCount();

      if($numero_registro!=0){
        session_start();

        $_SESSION["user"] = $_POST["username"];

        header("location:ViewPaciente.php");
      }else{
        header("location:login.php");
      }
    }catch(PDOException $e){
      die("Error: ".$e->getMessage());
    }
   ?>
</body>
</html>
